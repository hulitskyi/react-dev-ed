import React from 'react';
import { BrowserRouter, Link, Route, Switch } from 'react-router-dom';
import logo from './assets/logo.svg';
import { Home } from './templates/Home';
import { Edit } from './templates/Edit';
import { UserProfile } from './templates/UserProfile';
import {HeaderComponent} from "./templates/Components/HeaderComponent";

export function App() {
  return (
    <BrowserRouter>
      < HeaderComponent />

      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/edit">
          <Edit />
        </Route>
        <Route path="/user">
          <UserProfile />
        </Route>
      </Switch>
    </BrowserRouter>
  );
}
