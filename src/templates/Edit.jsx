import React from 'react';

export function Edit() {
  return (
      <div className="container mt-5">
        <div className="row">
          <div className="col-12 col-md-3 mt-md-5 mt-3">
            <div className="edit__profile-header border rounded bg-white">
              <div className="edit__profile-header-bg">
                <div className="container">
                  <div className="edit-profile-header__avatar-container">
                    <div className="edit-profile-header__avatar">
                    </div>
                  </div>
                </div>
              </div>
              <div className="container edit__user">
                <span className="edit__fullname font-weight-bold">User Name</span>
                <span className="edit__username">@username</span>
              </div>
            </div>
          </div>

          <div className="col-12 col-md-9 mt-md-5 mt-3">
            <div className="col border border-light shadow-sm bg-white mb-3 p-3">
              <form method="post" action="edit" id="form">
                <h3 className="mb-3">Personal Information</h3>
                <div className="form-group">
                  <div className="row">
                    <div className="col-md-6 mb-md-0 mb-3">
                      <input type="text" className="form-control edit__input-field" id="editFirstName"
                             placeholder="First Name" name="firstName" value="{{ data['firstname'] }}"/>
                        <span className="helper-text invalid-feedback" id="helper-text__FirstName"/>
                    </div>
                    <div className="col-md-6">
                      <input type="text" className="form-control edit__input-field" id="editLastName"
                             placeholder="Last Name" name="lastName" value="{{ data['lastname'] }}"/>
                        <span className="helper-text invalid-feedback" id="helper-text__LastName"/>
                    </div>
                  </div>
                </div>

                <div className="form-group">
                  <input type="email" className="form-control edit__input-field" id="editEmail" placeholder="Email"
                         name="email" value="{{ data['email'] }}"/>
                    <span className="helper-text invalid-feedback" id="helper-text__Email"/>
                </div>
                <div className="form-group">
                  <input type="text" className="form-control edit__input-field" id="editUserName"
                         aria-describedby="usernameHelp" placeholder="Username" name="userName"
                         value="{{ data['username'] }}"/>
                    <span className="helper-text invalid-feedback" id="helper-text__Username"/>
                    <small id="usernameHelp" className="form-text text-muted">Username must be at least three(3)
                      characters long</small>
                </div>
                <div className="form-group" id="bioFormGroup">
                  <textarea type="text" className="form-control edit__input-field" id="editBio"
                            aria-describedby="bioHelp" placeholder="Bio" name="bio"/>
                  <span className="helper-text invalid-feedback" id="helper-text__Bio"/>
                  <small id="bioHelp" className="d-inline form-text text-muted">Your bio goes here in max 1000
                    characters.
                    <span className="float-right" id="edit__bio-counter">0/1000</span>
                  </small>

                </div>
                <div className="form-group">
                  <input type="date" className="form-control edit__input-field" id="editDateOfBirth" value="2000-01-01"
                         name="dob"/>
                    <span className="helper-text invalid-feedback" id="helper-text__Dob"/>
                </div>
                <input type="button" className="edit__updatebtn" value=" Update " id="updatePersInfor"/>
              </form>

            </div>
            <div className="col edit-form border border-light shadow-sm bg-white mb-3 p-3">
              <h3 className="mb-3">Change Password</h3>
              <form>
                <div className="form-group">
                  <input type="password" className="form-control edit__input-field" id="editCurrentPassword"
                         placeholder="Current Password" name="currentPassword"/>
                    <span className="helper-text invalid-feedback" id="helper-text__currentPassword">Password is at least 6 characters long</span>
                </div>
                <div className="form-group">
                  <input type="password" className="form-control edit__input-field" id="editPassword"
                         placeholder="New Password" name="newPassword"/>
                    <span className="helper-text invalid-feedback" id="helper-text__password">This is invalid follow the helper.</span>
                    <small id="passwordHelp" className="form-text text-muted">Password must be at least six(6)
                      characters long and must contain at least one uppercase and number.</small>
                </div>
                <div className="form-group">
                  <input type="password" className="form-control edit__input-field" id="editConfirmPassword"
                         aria-describedby="passwordHelp" placeholder="Confirm Password" name="confirmPassword"/>
                    <span className="helper-text invalid-feedback" id="helper-text__confirmPassword">Passwords do not match</span>
                </div>
                <input type="button" className="edit__updatebtn" value=" Change password "/>
              </form>

            </div>
          </div>
        </div>
      </div>
  )
}
