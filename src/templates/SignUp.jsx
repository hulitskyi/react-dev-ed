import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';
import logo from '../assets/logo2.svg';

import { HeaderComponent } from './Components/HeaderComponent';
import { SignUpComponent } from './Components/SignUpComponent';

export function SignUp() {
  return (
    <>
      <HeaderComponent />
      <SignUpComponent />
    </>
  );
}
