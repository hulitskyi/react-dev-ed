import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';
import logo from '../assets/logo2.svg';
import { SignUp } from './SignUp';

export function Sign() {
  return (
    <Router>
      <Switch>
        <div className="twitter-contain">
          <div className="dev-svg-logo">
            <img className="logo-sign" src={logo} alt="" />
          </div>
          <div className="sign-up-txt">
            <h3>
              See what's happening in dev
              <span className="skobka">
                <span className="not-bold">communication</span>
              </span>{' '}
              right now
            </h3>
            <p>Join Us today.</p>
          </div>
          <div className="btn-container">
            <Link to="/signUp" className="sign-up-link sign-up-btn">
              <span>Sign up</span>
            </Link>
            <Link to="/login" className="login-link login-btn">
              <span>Login</span>
            </Link>
          </div>
        </div>

        <Route path="/signUp">
          <SignUp />
        </Route>
      </Switch>
    </Router>
  );
}
