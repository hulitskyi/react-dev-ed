import React from 'react';
import { AvatarContainer } from './Components/AvatarContainer';
import { HomeLeftSidebar } from './Components/HomeLeftSidebar';
import {HomeComponent} from "./Components/HomeComponent";

export function Home() {
  return (
    <>
      <AvatarContainer />
      <div className="container ">
        <div className="row">
          <div className="col-12 col-md-3">
            <HomeLeftSidebar />
          </div>
          <div className="col-12 col-md-9">
            <HomeComponent />
          </div>
        </div>
      </div>
    </>
  );
}
