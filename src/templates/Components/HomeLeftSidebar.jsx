import React from 'react';

export function HomeLeftSidebar() {
  return (
    <>
      <div className="profile-header">
        <h3 className="profile-element profile-element__fullname">Your Name</h3>
        <a className="text-primary profile-element profile-element__username ">@userName</a>

        <a href="#">Dialog</a>

        <p className="text-wrap profile-element profile-element__biography">
          Your bio goes here in max 140 characters. Lorem ipsum dolor sit amet, consectetur
          adipisicing elit, sed do eiusmod tempor incididunt ut lab.
        </p>
        <span className="profile-element profile-element__location">
          <i className="fas fa-map-marker-alt" />
          Your location
        </span>
        <span className="profile-element profile-element__website">
          <a href="" className="text-primary">
            <i className="fas fa-link"> Primary link</i>
          </a>
        </span>
      </div>

      <nav className="navbar navbar-expand-sm rounded border border-primary profile-search-user">
        <div className="input-group">
          <input
            className="form-control py-2 border-right-0 border form-control-header__search"
            type="search"
            placeholder="Search"
            id="example-search-input"
          />
          <span className="input-group-append">
            <button className="btn btn-outline-secondary border-left-0 border" type="button">
              <i className="fa fa-search" />
            </button>
          </span>
        </div>
      </nav>

      <div className="follow-panel bg-white rounded ">
        <span className="follow-panel__header border-bottom">
          Who to follow
          <a className="follow-panel__link text-primary d-inline-block">View all</a>
        </span>
        <ul className="list-group list-group-flush">
          <li className="list-group-item justify-content-space-between">
            <div className="row justify-content-around">
              <div className="col-lg-3 my-auto">
                <img src="" className="" alt="" />
              </div>
              <div className="col-lg-8">
                <span className="follow-panel__user-data text-wrap font-weight-bold d-block">
                  Giulio Bordonaro{' '}
                </span>
                <span className="follow-panel__user-data text-secondary font-weight-normal d-block">
                  @GiulioBx
                </span>
                <button type="button" className="btn btn-outline-primary follow-panel__button">
                  <i className="fas fa-user-plus follow-panel__follow-icon" /> Follow
                </button>
              </div>
            </div>
          </li>
          <li className="list-group-item">
            <div className="row justify-content-around">
              <div className="col-lg-3">
                <img src="" alt="" />
              </div>
              <div className="col-lg-8">
                <span className="follow-panel__user-data text-wrap font-weight-bold d-block">
                  Interesting User{' '}
                </span>
                <span className="follow-panel__user-data text-secondary font-weight-normal d-block">
                  @User
                </span>
                <button type="button" className="btn btn-outline-primary follow-panel__button">
                  <i className="fas fa-user-plus follow-panel__follow-icon" /> Follow
                </button>
              </div>
            </div>
          </li>
          <li className="list-group-item">
            <div className="row justify-content-around">
              <div className="col-lg-3">
                <img src="" alt="" />
              </div>
              <div className="col-lg-8">
                <span className="follow-panel__user-data text-wrap font-weight-bold d-block">
                  Other User{' '}
                </span>
                <span className="follow-panel__user-data text-secondary font-weight-normal d-block">
                  @Other User
                </span>
                <button type="button" className="btn btn-outline-primary follow-panel__button">
                  <i className="fas fa-user-plus follow-panel__follow-icon" /> Follow
                </button>
              </div>
            </div>
          </li>
        </ul>
        <div className="container row">
          <div className="col mt-2">
            <a className="text-primary follow-panel__link">Find friends</a>
          </div>
        </div>
      </div>
      <div className="hashtag-panel bg-white rounded ">
        <span className="hashtag__header">Worldwide Trends</span>
        <ul className="hashtag-list">
          <li className="hashtag-list__hashtag">
            <a className="text-primary">#NewTwitter</a>
          </li>
          <li className="hashtag-list__hashtag">
            <a className="text-primary">#GBX</a>
          </li>
          <li className="hashtag-list__hashtag">
            <a className="text-primary">#graphicdesign</a>
          </li>
          <li className="hashtag-list__hashtag">
            <a className="text-primary">#mockup</a>
          </li>
          <li className="hashtag-list__hashtag">
            <a className="text-primary">#layout</a>
          </li>
          <li className="hashtag-list__hashtag">
            <a className="text-primary">#PSD</a>
          </li>
          <li className="hashtag-list__hashtag">
            <a className="text-primary">#FreeTemplate</a>
          </li>
          <li className="hashtag-list__hashtag">
            <a className="text-primary">#Lorem</a>
          </li>
          <li className="hashtag-list__hashtag">
            <a className="text-primary">#ipsum</a>
          </li>
          <li className="hashtag-list__hashtag">
            <a className="text-primary">#amet</a>
          </li>
        </ul>
      </div>
    </>
  );
}
