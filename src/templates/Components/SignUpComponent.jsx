import React from 'react';
import logo from '../../assets/logo2.svg';

export function SignUpComponent() {
  return (
    <main className="registration-page">
      <div className="registr-container">
        <div className="registr-bord">
          <p className="registr-bord__title">Create an account</p>
          <form action="signup" method="post" id="form">
            <div className="registr-line registr-first">
              <div className="d-block reg-style-2  reg-style-3">
                <input
                  id="FirstName"
                  type="text"
                  name="firstName"
                  className="reg-style "
                  placeholder="First Name"
                  value=""
                />
                <p id="fNameAdvice" className="advice" />
              </div>
              <div className="d-block reg-style-2">
                <input
                  id="LastName"
                  type="text"
                  name="lastName"
                  className="reg-style  "
                  placeholder="Last Name"
                  value=""
                />
                <p id="lNameAdvice" className="advice" />
              </div>
            </div>
            <div className="registr-line">
              <input
                id="Email"
                type="text"
                name="email"
                className="reg-style"
                placeholder="Email"
                value=""
              />
              <p id="emailAdvice" className="advice" />
            </div>
            <div className="registr-line">
              <input
                id="Username"
                type="text"
                name="userName"
                className="reg-style"
                placeholder="Username"
                value=""
              />
              <br />
              <p id="usernameAdvice" className="advice" />
            </div>
            <div className="registr-line">
              <input
                id="Date"
                type="text"
                name="dob"
                className="reg-style reg-date"
                onFocus="(this.type='date')"
                placeholder="Date of birth"
                value=""
              />
              <p id="dateAdvice" className="advice" />
            </div>
            <div className="registr-line">
              <input
                id="Password"
                type="password"
                name="password"
                className="reg-style"
                placeholder="Password"
              />
              <br />
              <p id="passwordAdvice" className="advice" />
            </div>
            <div className="registr-line">
              <input
                id="Confirm"
                type="password"
                name="confirmPassword"
                className="reg-style"
                placeholder="Confirm password"
              />
              <p id="confirmAdvice" className="advice" />
            </div>
            <div className="registr-submit">
              <input type="button" value="SUBMIT" className="registr-submit__btn" />
            </div>
          </form>
        </div>
        <div className="registr-footer ">
          <p className="registr-footer__question ">Already using Dev Communication?</p>
          <a href="login" className="registr-footer__log-href">
            LOGIN
          </a>
        </div>
      </div>
    </main>
  );
}
