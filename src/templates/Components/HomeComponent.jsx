import React from 'react';

export function HomeComponent() {
  return (
    <nav className="navbar navbar-expand navbar-light navbar-custom">
      <div className="tweets__header-container">
        <div className="container">
          <div className="row w-100 border-bottom tweets__header__first-row">
            <div className="tweets__header__carousel">
              <div
                className="tweets__header__nav-item"
                data-active="true"
                data-tab_target_class="tweets__body-container"
              >
                <div className="tweets_header__title">Tweet</div>
                <div className="tweets_header__number" data-active="true">
                  100
                </div>
                <div className="underline" data-active="true" />
              </div>
              <div className="tweets__header__nav-item" data-active="false">
                <div className="tweets_header__title">Photos/Videos</div>
                <div className="tweets_header__number" data-active="false">
                  62
                </div>
                <div className="underline" data-active="false" />
              </div>
              <div
                className="tweets__header__nav-item"
                data-active="false"
                data-tab_target_class="following__body-container"
              >
                <div className="tweets_header__title">Following</div>
                <div className="tweets_header__number" data-active="false">
                  164
                </div>
                <div className="underline" data-active="false" />
              </div>
              <div
                className="tweets__header__nav-item"
                data-active="false"
                data-tab_target_class="following__body-container"
              >
                <div className="tweets_header__title">Followers</div>
                <div className="tweets_header__number" data-active="false">
                  277
                </div>
                <div className="underline" data-active="false" />
              </div>
              <div className="tweets__header__nav-item" data-active="false">
                <div className="tweets_header__title">Favorites</div>
                <div className="tweets_header__number" data-active="false">
                  24
                </div>
                <div className="underline" data-active="false" />
              </div>
              <div className="tweets__header__nav-item" data-active="false">
                <div className="tweets_header__title">View</div>
                <div className="tweets_header__number" data-active="false">
                  Lists
                </div>
                <div className="underline" data-active="false" />
              </div>
            </div>

            <div className="col-auto ml-auto following-area align-items-center justify-content-end">
              <button type="button" className="btn btn-primary btn-sm following-button">
                Following
              </button>
              <div
                className="settings"
                id="settingsDropdown"
                data-toggle="dropdown"
                aria-haspopup="false"
                aria-expanded="false"
              >
                <i className="fas fa-cog settings__gear-icon" />
                <i className="fas fa-chevron-down settings__arrow-icon" />
              </div>
              <div className="dropdown-menu settings-menu" aria-labelledby="settingsDropdown">
                <a className="dropdown-item" href="edit">
                  Edit
                </a>
                <a className="dropdown-item" href="sign">
                  Log Out
                </a>
              </div>
            </div>
          </div>
          <div className="row mr-auto border-bottom w-100 tweets__header__second-row align-items-center">
            <div className="col-auto tweets__header__nav-item-second-row" data-active="true">
              {' '}
              Tweets
            </div>
            <div className="col-auto tweets__header__nav-item-second-row" data-active="false">
              {' '}
              Tweets and replies
            </div>
          </div>
        </div>
      </div>
    </nav>
  );
}
