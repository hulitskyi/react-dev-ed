import React from 'react';
import logo from '../../assets/logo2.svg';
import {BrowserRouter, Link, Switch, Route} from "react-router-dom";

export function HeaderComponent() {
  return (
      <nav className="navbar navbar-expand-md navbar-light bg-light shadow-sm fixed-top">
          <div className="container navbar-container__header">
            <button
                className="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbar-header"
                aria-controls="navbar-header"
                aria-expanded="false"
                aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon" />
            </button>
            <div className="collapse navbar-collapse" id="navbar-header">
              <ul className="navbar-nav mr-auto">
                <li className="nav-item">
                  <Link to="/" className="nav-link">
                    <i className="fas fa-home" /> Home
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to="" className="nav-link">
                    <i className="fas fa-at" /> Connect
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to="" className="nav-link">
                    <i className="fas fa-hashtag" /> Discover
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to="/user" className="nav-link">
                    <i className="fas fa-user" /> Me
                  </Link>
                </li>
                <li className="nav-item nav-item__settings dropdown">
                  <a
                      href=""
                      className="nav-link dropdown-toggle"
                      id="dropDownHeader2"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                  >
                    <i className="fas fa-cog" /> Settings
                  </a>
                  <div className="dropdown-menu" aria-labelledby="dropDownHeader2">
                    <Link className="dropdown-item" to="/edit">
                      Edit
                    </Link>
                    <Link className="dropdown-item" to="/logOut">
                      Logout
                    </Link>
                  </div>
                </li>
              </ul>
              <Link to="/" className="navbar-brand mx-auto navbar-header__logo">
                <i className="fab fa-twitter " />
              </Link>

              <ul className="navbar-nav ml-auto navbar-header__left-nav">
                <li className="nav-item">
                  <div className="input-group">
                    <input
                        className="form-control py-2 border-right-0 border form-control-header__search"
                        type="search"
                        placeholder="Search"
                        id="example-search-input"
                    />
                    <span className="input-group-append">
                    <button
                        className="btn btn-outline-secondary border-left-0 border"
                        type="button"
                    >
                      <i className="fa fa-search" />
                    </button>
                  </span>
                  </div>
                </li>
                <li className="nav-item">
                  <Link to="/chatList" className="nav-link">
                    <i className="fas fa-envelope" />
                  </Link>
                </li>
                <li className="nav-item dropdown">
                  <a
                      href=""
                      className="nav-link dropdown-toggle"
                      id="dropDownHeader"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                  >
                    <i className="fas fa-cog" />
                  </a>
                  <div className="dropdown-menu" aria-labelledby="dropDownHeader">
                    <Link className="dropdown-item" to="/edit">
                      Edit
                    </Link>
                    <Link className="dropdown-item" to="/logOut">
                      Logout
                    </Link>
                  </div>
                </li>

                <li className="nav-item">
                  <Link to="/" className="nav-link  navbar-nav__compose-button">
                    <i className="fas fa-feather-alt" />
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
  );
}
