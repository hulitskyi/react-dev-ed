import React from 'react';

export function AvatarContainer() {
  return (
    <div className="profile-background">
      <div className="container">
        <div className="avatar-container">
          <div className="avatar" />
        </div>
      </div>
    </div>
  );
}
